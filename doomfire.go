package main

import (
	"flag"
	"math"
	"math/rand"
	"strings"
	"time"

	"github.com/gdamore/tcell"
	"github.com/mbndr/figlet4go"
)

var rgbs []uint8 = []uint8{
	0x07, 0x07, 0x07,
	0x1F, 0x07, 0x07,
	0x2F, 0x0F, 0x07,
	0x47, 0x0F, 0x07,
	0x57, 0x17, 0x07,
	0x67, 0x1F, 0x07,
	0x77, 0x1F, 0x07,
	0x8F, 0x27, 0x07,
	0x9F, 0x2F, 0x07,
	0xAF, 0x3F, 0x07,
	0xBF, 0x47, 0x07,
	0xC7, 0x47, 0x07,
	0xDF, 0x4F, 0x07,
	0xDF, 0x57, 0x07,
	0xDF, 0x57, 0x07,
	0xD7, 0x5F, 0x07,
	0xD7, 0x5F, 0x07,
	0xD7, 0x67, 0x0F,
	0xCF, 0x6F, 0x0F,
	0xCF, 0x77, 0x0F,
	0xCF, 0x7F, 0x0F,
	0xCF, 0x87, 0x17,
	0xC7, 0x87, 0x17,
	0xC7, 0x8F, 0x17,
	0xC7, 0x97, 0x1F,
	0xBF, 0x9F, 0x1F,
	0xBF, 0x9F, 0x1F,
	0xBF, 0xA7, 0x27,
	0xBF, 0xA7, 0x27,
	0xBF, 0xAF, 0x2F,
	0xB7, 0xAF, 0x2F,
	0xB7, 0xB7, 0x2F,
	0xB7, 0xB7, 0x37,
	0xCF, 0xCF, 0x6F,
	0xDF, 0xDF, 0x9F,
	0xEF, 0xEF, 0xC7,
	0xFF, 0xFF, 0xFF}

var firePixels []int
var width int
var height int
var totalPixels int
var outMode int
var quit bool = false
var freezeFire bool = false

func initFire(s tcell.Screen) {
	width, height = s.Size()
	totalPixels = width * height
	firePixels = make([]int, width*height, width*height)
	for x := 0; x < width*height; x++ {
		firePixels[x] = 0
	}
	setFire(true)
}

func setFire(state bool) {
	for x := 0; x < width; x++ {
		if state {
			firePixels[(height-1)*width+x] = 36
		} else {
			firePixels[(height-1)*width+x] = 0
		}
	}
}

func toggleFire() {
	setFire(firePixels[totalPixels-1] == 0)
}

func genMsg(msg string) string {
	ascii := figlet4go.NewAsciiRender()
	options := figlet4go.NewRenderOptions()
	options.FontName = "banner3"
	ascii.LoadFont("./fonts/")
	rendered, err := ascii.RenderOpts(msg, options)
	if err != nil {
		panic(err)
	}
	return rendered
}

func setMsg(msg string) {
	fig_arr := strings.Split(genMsg(msg), "\n")
	for y, line := range fig_arr {
		for x, char := range line {
			if char != ' ' {
				x_offset := int(math.Round((float64(width) / float64(2)) - (float64(len(line)) / float64(2))))
				y_offset := int(math.Round((float64(height) / float64(2)) - (float64(len(fig_arr)) / float64(2))))

				firePixels[(y+y_offset)*width+x+x_offset] = 36
			}
		}
	}
}

func doFire(message string) {
	for x := 0; x < width; x++ {
		for y := 1; y < height; y++ {
			spreadFire(y*width + x)
		}
	}
	setMsg(message)
}

func spreadFire(src int) {
	rnd := rand.Intn(3) & 3
	dst := src - rnd + 1
	if dst-width < 0 {
		dst = width
	} else if dst >= totalPixels {
		dst = totalPixels - 1
	}
	firePixels[dst-width] = firePixels[src] - (rnd & 3)
}

func printFire(s tcell.Screen) {
	st := tcell.StyleDefault
	for x := 0; x < width; x++ {
		for y := 0; y < height; y++ {
			val := firePixels[(y*width+x)] * 3
			if val < 0 {
				val = 0
			}
			switch outMode {
			case 1:
				r, g, b := math.Round((float64(rgbs[val])/float64(255))*5.0),
					math.Round((float64(rgbs[val+1])/float64(255))*5.0),
					math.Round((float64(rgbs[val+2])/float64(255))*5.0)
				c := tcell.Color(16 + (36 * r) + (6 * g) + b)
				st = st.Background(c)
			case 2:
				c := tcell.NewRGBColor(int32(rgbs[val]), int32(rgbs[val+1]), int32(rgbs[val+2]))
				st = st.Background(c)
			}
			s.SetCell(x, y, st, ' ')
		}
	}
	s.Show()
}

func handleInput(s tcell.Screen) {
	for {
		ev := s.PollEvent()
		switch ev := ev.(type) {
		case *tcell.EventKey:
			switch ev.Key() {
			case tcell.KeyCtrlC:
				quit = true
			default:
				switch ev.Rune() {
				case 'q':
					quit = true
				case 'f':
					freezeFire = !freezeFire
				case ' ':
					toggleFire()
				}
			}
		case *tcell.EventResize:
			width, height = s.Size()
			initFire(s)
		}
	}
}

func fireLoop(s tcell.Screen, fireTickLength int, message string) {
	for {
		if quit {
			break
		}
		if !freezeFire {
			doFire(message)
		}
		printFire(s)
		time.Sleep(time.Duration(fireTickLength) * time.Millisecond)
	}
}

func main() {

	message := flag.String("m", "", "Message to show in the center of the fire")
	ticklen := flag.Int("t", 30, "Time to wait between frames")
	colormode := flag.Int("c", 1, "The colormode used for output: 1 => 8-Bit 2=24-Bit")
	flag.Parse()

	s, e := tcell.NewScreen()
	if e != nil {
		panic(e)
	}
	if e = s.Init(); e != nil {
		panic(e)
	}
	defer s.Fini()

	outMode = *colormode

	initFire(s)
	go handleInput(s)
	fireLoop(s, *ticklen, *message)
}
