module gitlab.com/joru/doomfire

go 1.16

require (
	github.com/gdamore/tcell v1.3.0
	github.com/lucasb-eyer/go-colorful v1.0.3 // indirect
	github.com/mattn/go-runewidth v0.0.8 // indirect
	github.com/mbndr/figlet4go v0.0.0-20190224160619-d6cef5b186ea
	golang.org/x/sys v0.0.0-20200124204421-9fbb57f87de9 // indirect
	golang.org/x/text v0.3.2 // indirect
)
